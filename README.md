# test-automation-dashboard

### Pre-installation requirements:
* To run this project quickly and without hassle:
  * Docker Desktop (https://www.docker.com/products/docker-desktop/)
* To do some development on this project:
  * NodeJS (https://nodejs.org/)
* To inspect your MongoDB database:
  * MongoDB Compass (https://www.mongodb.com/try/download/compass)

### First step:
1. Clone this repository.

### To install and run the api:
1. `cd app`
2. `docker-compose up`
3. Go to http://localhost:3000/. This should return a Hello World!

### To install and run the dashboard:
1. `cd frontend`
2. `npm install`
3. `npm start`

### POSTing results to the API that show up in the dashboard:
1. POST a request to http://localhost:3000/api/results
2. In the body, use these values as an example:
```
{
    "project": "test-set-1",
    "environment": "acceptance",
    "status": "passed",
    "totalTests": 100,
    "passedTests": 90,
    "failedTests": 0,
    "skippedTests": 10
}
```
