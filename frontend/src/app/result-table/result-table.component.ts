import { Component, Input, OnInit } from '@angular/core';
import { ResultSet } from '../resultSet';
import { MongoDbService } from '../services/mongo-db.service';

@Component({
  selector: 'app-result-table',
  templateUrl: './result-table.component.html',
  styleUrls: ['./result-table.component.css'],
})
export class ResultTableComponent implements OnInit {
  interval: any;
  latestResult: ResultSet = {
    _id: '',
    project: 'err',
    environment: '-',
    status: 'No results',
    totalTests: 0,
    passedTests: 0,
    failedTests: 0,
    skippedTests: 0,
    createdAt: '',
    updatedAt: '',
    __v: 0,
  };

  @Input('projectName') projectName: string;
  @Input('reportUrl') reportUrl: string;
  @Input('dataSet') dataSet: string;

  constructor(private mongoDbService: MongoDbService) {
  }

  formatDate(updatedAt: string): string {
    if (
        updatedAt === '' ||
        updatedAt === undefined ||
        updatedAt === 'undefined' ||
        updatedAt === 'Never'
    ) {
      return 'Never';
    } else {
      return new Date(updatedAt).toLocaleDateString('nl-nl', {
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
      });
    }
  }

  showWarningIfOutDated(updatedAt: string): any {
    if ((new Date().getTime() - new Date(updatedAt).getTime()) / (1000 * 60 * 60 * 24) > (1 - (2 / 24))) {
      return ' \u26A0';
    } else {
      return '';
    }
  }

  retrieveResults(): void {
    this.mongoDbService
        .getLatestFromDataSet(this.dataSet)
        .subscribe((result) => (this.latestResult = result));
  }

  ngOnInit(): void {
    this.retrieveResults();
    this.interval = setInterval(() => {
      this.retrieveResults();
    }, 20000);
  }

}
