import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResultSet } from '../resultSet';

@Injectable({
  providedIn: 'root',
})
export class MongoDbService {
  private baseUrl = 'http://localhost:3000/';
  private latestResultsEndpoint = 'results/latest/';

  constructor(private http: HttpClient) {}

  getLatestFromDataSet(dataSet: string): Observable<ResultSet> {
    return this.http.get<ResultSet>(this.baseUrl + this.latestResultsEndpoint, {
      params: { project: dataSet },
    });
  }
}
