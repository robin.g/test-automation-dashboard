import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs';

@Component({
    selector: 'app-ta-dashboard',
    templateUrl: './ta-dashboard.component.html',
    styleUrls: ['./ta-dashboard.component.css'],
})
export class TaDashboardComponent {
    /** Based on the screen size, switch from standard to one column per row */
    cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
        map(({matches}) => {
            return [
                {
                    title: 'Team Blue',
                    cols: 1,
                    rows: 1,
                    testSuites: [
                        {
                            name: 'frontend',
                            dataSet: 'test-set-1',
                            url: 'about:blank'
                        },
                    ],
                },
                {
                    title: 'Team Red',
                    cols: 1,
                    rows: 1,
                    testSuites: [
                        {
                            name: 'frontend',
                            dataSet: 'test-set-2',
                            url: 'about:blank'
                        },

                    ],
                },
                {
                    title: 'Team Green',
                    cols: 1,
                    rows: 1,
                    testSuites: [
                        {
                            name: 'backend',
                            dataSet: 'test-set-3',
                            url: 'about:blank'
                        },
                    ],
                }
            ];
        })
    );

    constructor(private breakpointObserver: BreakpointObserver) {
    }
}
