export interface ResultSet {
  _id:            string,
  project:        string,
  environment:    string,
  status:         string,
  totalTests:     number,
  passedTests:    number,
  failedTests:    number,
  skippedTests:   number,
  createdAt:      string,
  updatedAt:      string,
  __v:            number
}
