import express, { request, response } from 'express';
import { connectToDatabase } from './services/database.service';
import { resultsRouter } from './routes/results.router';

/**
 * Got most of this from https://www.mongodb.com/compatibility/using-typescript-with-mongodb-tutorial
 */

const app = express();
const port = 3000; // default port to listen

connectToDatabase()
    .then(() => {
        app.get("/", (request, response) => {
           response.send('Hello world!')
        });

        app.use("/results", resultsRouter);

        app.listen(port, () => {
            console.log(`Server started at http://localhost:${port}`);
        });
    })
    .catch((error: Error) => {
        console.error("Database connection failed", error);
        process.exit();
    });
