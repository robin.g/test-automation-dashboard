import { Collection, Db, MongoClient } from 'mongodb';

const serverAddress = 'mongodb://mongodb'
export const collections: { results?: Collection } = {}

export async function connectToDatabase() {
    const client: MongoClient = new MongoClient(serverAddress);
    await client.connect();

    const database: Db = client.db('data');
    collections.results = database.collection('results');
}
