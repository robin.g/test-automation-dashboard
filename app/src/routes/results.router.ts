import express, { Request, Response } from 'express';
import TestResult from '../models/testResult';
import { collections } from '../services/database.service';

export const resultsRouter = express.Router();
resultsRouter.use(
    express.json()
);
resultsRouter.use(
    function(req, res, next) {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', 'true');

        // Pass to next layer of middleware
        next();
    }
);

resultsRouter.get('/', async (request: Request, response: Response) => {
    try {
        const results = (await collections.results.find({}).toArray()) as TestResult[];
        response.status(200).send(results);
    } catch (error) {
        response.status(500).send(error.message);
    }
});

resultsRouter.post('/', async (request: Request, response: Response) => {
    try {
        const newTestResult = request.body as TestResult;
        const timestamp = new Date().toISOString();
        newTestResult.createdAt = timestamp;
        newTestResult.updatedAt = timestamp;
        const databaseInsertion = await collections.results.insertOne(newTestResult);

        if (databaseInsertion) {
            response.status(201).send('Successfully saved test result.');
        } else {
            response.status(500).send('Failed to save test result.');
        }
    } catch (error) {
        console.error(error);
        response.status(400).send(error.message);
    }
});

resultsRouter.get('/latest/', async function(request: Request, response: Response) {
    let result: TestResult;

    if (request.query.project) {
        result = (await collections.results.findOne({project: request.query.project}, {sort: {createdAt: -1}})) as TestResult;
    } else {
        result = (await collections.results.findOne({}, {sort: {createdAt: -1}})) as TestResult;
    }

    if (result) {
        response.status(200).send(result);
    } else {
        response.status(400).send(`No results with project ${request.query.project}. Please provide an existing project.`);
    }
});
