import { ObjectId } from 'mongodb';

export default class TestResult {
    constructor(
        public project: string,
        public environment: string,
        public status: string,
        public totalTests: number,
        public passedTests: number,
        public failedTests: number,
        public skippedTests: number,
        public _id?: ObjectId,
        public createdAt?: string,
        public updatedAt?: string,
    ) {
    }
}